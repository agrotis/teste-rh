# Teste Front-end Plataforma

Teste para Front end do time plataforma.

## Descrição

O teste consiste em construir um formulário simples com o seguinte [template](https://www.figma.com/proto/PXi5PcZWks8Z7veqA8WU30/Teste-Front-end).

Criar um repositório aberto no github e enviar o link por e-mail ao completar o teste.

## Requisitos

- Desenvolver com React.

- Construção do formulário funcional.

- Seguir com fidelidade o prototipo.

- Dar um console.log dos dados de envio.

- Usar MATERIAL-UI para components default (icones/inputs/frames).

- Consumir os endpoint abaixo para popular os inputs de select.

	- [Endpoint laboratórios](https://bitbucket.org/agrotis/teste-rh/raw/3bc797776e54586552d1c9666fd7c13366fc9548/teste-front-end-1/laboratorios.json)
  
	- [Endpoint propriedades](https://bitbucket.org/agrotis/teste-rh/raw/3bc797776e54586552d1c9666fd7c13366fc9548/teste-front-end-1/propriedades.json)
  
- Envio do formulário deve ter o seguinte formato:

```

{
	nome: 'Jon doe',
	dataInicial: '2022-02-02T17:41:44Z',
	dataFinal: '2022-02-02T17:41:44Z',
	infosPropriedade: {
		id: 1,
		nome: 'Fazenda Agrotis'
	},
	cnpj: '79.200.214/0001-61',
	laboratorio: {
		id: 3,
		nome: 'Osborne Agro'
	},
	observacoes: 'Observacao exemplo de teste'
}
```

## Itens Opcionais

- Usar styled-componts.

- Usar uma lib de formulário (react-hook-form por exemplo).

- Utilizar hooks.

- Typescript
